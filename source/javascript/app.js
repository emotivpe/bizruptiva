$(document).ready(function () {
    $(".menu_on").click(function () {
        $("#header > .menu").fadeIn("fast");
        $('#header > .menu-blanco').attr('style', 'display:none !important');
        
    });

    $(".close").click(function () {
        $(".menu").fadeOut("fast");
    });

    $(".open-close").click(function () {
        $(this).toggleClass('close-open');
        $(".sub-menu").slideToggle("slow");
    });

    $(".btn-header").click(function () {
        $('html, body').animate({
            scrollTop: $("#discover").offset().top
        }, 1000);
    });

    $(".btn-discover").click(function () {
        $('html, body').animate({
            scrollTop: $("#proyectos").offset().top
        }, 1000);
    });

    $(".btn-proyectos").click(function () {
        $('html, body').animate({
            scrollTop: $("#bizruptores").offset().top
        }, 1000);
    });
    $(".btn-footer").click(function () {
        $('html, body').animate({
            scrollTop: $("#header").offset().top
        }, 1000);
    });

    $(window).resize(function () {
        viewportWidth = $(window).width();
        if (viewportWidth < 768) {
            $('.menu_on').addClass('menu_on_m');
            $(".menu_on_m").click(function () {
                $('#header > .menu-blanco').attr('style', 'display:block !important');
            });

            $(".close").click(function () {
                $(".menu-blanco").fadeOut("fast");
            });

        } else {
            $('.menu_on').removeClass('menu_on_m');
            $('.menu').attr('style', '');
            $('#header > .menu-blanco').attr('style', 'display:none !important'); 
           
        }
    });

    $(window).width(function () {
        viewportWidth = $(window).width();
        if (viewportWidth < 768) {
            $('.menu_on').addClass('menu_on_m');
            $(".menu_on_m").click(function () {
                $('#header>.menu-blanco').attr('style', 'display:block !important');
               
            });

            $(".close").click(function () {
                $(".menu-blanco").fadeOut("fast");
                
            });
        } else {
            $('.menu_on').removeClass('menu_on_m');
            $('.menu').attr('style', '');
            $('#header>.menu-blanco').attr('style', 'display:none !important');

        }
    });
});